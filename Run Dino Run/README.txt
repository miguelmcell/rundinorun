Group Number: 3
Group Members:
    Miguel Mendoza
    Gage Courtaway
    Shayan Khan
    Jianbin Lin

Contributions:
Miguel Mendoza (Release X%, Overall X%):
    * Learned the SpriteKit library for game
    * Entirety of game screen
    * Entirety of store screen
    * Game Assets
    * CoreData protocols for Currency and Inventory
    * Delegates for Currency and Inventory manager
    * Added customization screen 
    * Dark mode
    * tap/tap and hold gestures for game
    * Dynamic level generation for game
    * Dynamic speed further the player proceeds
    * Replaced my coredata segments with firebase
    * Created player preference firebase table to save player's color, current theme, and current level selected
    * built sound effects using audacity
    * photoshopped sprite sheets for Dino blinking animation and cloud types for dark mode
Gage Courtaway (Release X%, Overall X%):
    * Landscape mode support for screens
    * Constraints and storyboard design
    * Powerpoint slide presentation
    * Dark mode
    * Debugging and bug fixes
    * Moral and emotional support during these trying times
Shayan Khan (Release X%, Overall X%):
    * Loading screen
    * Second loading screen for animation
    * Added animations to make the dinosaur image travel from the loading screen to the home screen 
    * Leaderboard
    * Dark mode for leaderboard
Jianbin Lin (Release X%, Overall X%):
    * Entirety of Login/Sign Up Screens
        * Includes remembering signed in users
	* welcome-back message and logging-in message
    * Entirety of setting screen
	* Username display
	* Reset password
	* Log out
	* Delete account
    * Setting Up screens/segues
    * Setting up custom view controllers with their appropriate files
    * Normalized firebase tables

Differences:
    * Login/Sign up implemented with firebase instead of appleId or google accounts as we found it to be more convenient and simple to follow
    * Loading screen is a static screen so we could not implement a loading bar. The loading bar would have been added to the second loading screen but it defeats the purpose since the app is already loaded. 
    * Removed avatars and item accessories, not enough time to animate them

Note: Please use iPhone 11 Pro Max configuration for the stimulator 