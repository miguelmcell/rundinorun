//
//  CustomizeCollectionViewCell.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/29/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit

class CustomizeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    
}
