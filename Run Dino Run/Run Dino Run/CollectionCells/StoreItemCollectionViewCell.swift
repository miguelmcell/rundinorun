//
//  StoreItemCollectionViewCell.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/6/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit

class StoreItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var itemSoldImage: UIImageView!
    
}
