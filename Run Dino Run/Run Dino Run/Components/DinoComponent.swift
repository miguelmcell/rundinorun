//
//  DinoComponent.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/9/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import SpriteKit
import GameplayKit

class DinoComponent: GKComponent {
    
    var curCoins = 0
    override init() {
        super.init()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func update(deltaTime seconds: TimeInterval) {
        super.update(deltaTime: seconds)
    }
}
