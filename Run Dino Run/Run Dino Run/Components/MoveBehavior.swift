//
//  MoveBehavior.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/9/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import GameplayKit
import SpriteKit

class MoveBehavior: GKBehavior {
    
    init (targetSpeed: Float, seek: GKAgent, avoid: [GKAgent]) {
        super.init()
        
        setWeight(1.0, for: GKGoal(toReachTargetSpeed: targetSpeed))
        
        setWeight(0.5, for: GKGoal(toSeekAgent: seek))
        
//        setWeight(1.0, for: GKGoal(toAvoid: avoid, maxPredictionTime: 1.0))
    }
}
