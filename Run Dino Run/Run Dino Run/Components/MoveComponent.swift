//
//  MoveComponent.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/9/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import SpriteKit
import GameplayKit

class MoveComponent: GKAgent2D, GKAgentDelegate {
    let entityManager: EntityManager
    
    init(maxSpeed: Float, maxAcceleration: Float, radius: Float, entityManger: EntityManager) {
        self.entityManager = entityManger
        super.init()
        self.maxSpeed = maxSpeed
        self.maxAcceleration = maxAcceleration
        self.radius = radius
        self.mass = 0.01
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func agentWillUpdate(_ agent: GKAgent) {
        guard let spriteComponent = entity?.component(ofType: SpriteComponent.self) else {
            return
        }
        position = vector2(Float(spriteComponent.node.position.x), Float(spriteComponent.node.position.y))
    }
    
    func agentDidUpdate(_ agent: GKAgent) {
        guard let spriteComponent = entity?.component(ofType: SpriteComponent.self) else {
            return
        }
        spriteComponent.node.position = CGPoint(x: CGFloat(position.x), y: CGFloat(position.y))
    }
    
    override func update(deltaTime seconds: TimeInterval) {
//        guard let entity = entity,
//            let dinoComponent = entity.component(ofType: DinoComponent.self) else {
//                return
//        }
//        
//        let moveComponents = entityManager.moveComponents()
//        for moveComponent in moveComponents {
//            print("ayy setting a moveBehaviour")
//            behavior = MoveBehavior(targetSpeed: speed, seek: moveComponent, avoid: [])
//            break
//        }
        // get first component and make it its target or print what it is
    }
}
