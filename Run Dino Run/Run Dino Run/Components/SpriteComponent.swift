//
//  SpriteComponent.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/9/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import SpriteKit
import GameplayKit

class SpriteComponent: GKComponent {
    let dinoCategory: UInt32 = 0x1 << 0
    let cactusCategory: UInt32 = 0x1 << 1
    let node: SKSpriteNode
    
    init(texture: SKTexture, collisionCategory: UInt32) {
        node = SKSpriteNode(texture: texture, color: .white, size: texture.size())
        if(collisionCategory == dinoCategory) {
        //            node.physicsBody?.affectedByGravity = false
                    node.physicsBody = SKPhysicsBody(circleOfRadius: 25)
        }
        else {
            node.physicsBody = SKPhysicsBody(circleOfRadius: 18*0.80)
        }
        node.physicsBody?.usesPreciseCollisionDetection = true
        node.physicsBody?.affectedByGravity = false
        node.physicsBody?.categoryBitMask = collisionCategory
        node.physicsBody?.contactTestBitMask = dinoCategory | cactusCategory
        node.physicsBody?.collisionBitMask = 0
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
