//
//  CustomViewCell.swift
//  Run Dino Run
//
//  Created by XCodeClub on 2020-05-03.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit

class CustomViewCell: UITableViewCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var level: UILabel!
    @IBOutlet weak var star: UIImageView!
    @IBOutlet weak var rank: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
