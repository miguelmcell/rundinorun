//
//  CustomizeViewController.swift
//  Run Dino Run
//
//  Created by Jianbin Lin on 3/30/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import Firebase
import SpriteKit
import AVFoundation

class CustomizeViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var skView: SKView!
    @IBOutlet weak var dinoImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var desertLabel: UILabel!
    @IBOutlet weak var desertButton: UIButton!
    @IBOutlet weak var nightLabel: UILabel!
    @IBOutlet weak var nightButton: UIButton!
    
    var user = Auth.auth().currentUser!
    var db = Firestore.firestore()
    var colorItems:[StoreItem] = [StoreItem(itemName: "Default", itemImage: "", itemPrice: 0, isSold: true, itemCategory: "colors")]
    var pages = 0
    var frame = CGRect.zero
    var timer = Timer()
    var dino = SKSpriteNode()
    var audioPlayer: AVAudioPlayer?
    
    @IBAction func desertOnPress(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
        let docRef = db.collection("Users").document(user.uid)
        docRef.updateData(["level" : "desert"])
    }
    
    @IBAction func nightOnPress(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
        let docRef3 = db.collection("Users").document(user.uid)
        docRef3.updateData(["level" : "night"])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCustomize", for: indexPath) as! CustomizeCollectionViewCell
        let row = indexPath.row
        cell.itemName.text = colorItems[row].itemName
        cell.itemImage.backgroundColor = getColor(color: colorItems[row].itemName)
        
        return cell
    }
    
    // green brown aqua lime gold ghost
    func getColor(color: String) -> SKColor {
        switch color {
        case "Midnight":
            return SKColor.black
        case "Orange":
            return SKColor.orange
        case "Pink":
            return SKColor.systemPink
        case "Blue":
            return SKColor.blue
        case "Yellow":
            return SKColor.yellow
        case "Ghost":
            return SKColor.clear
        case "Default":
            return SKColor.gray
        case "Purple":
            return SKColor.purple
        case "Red":
            return SKColor.red
        case "Green":
            return SKColor.green
        case "Brown":
            return SKColor.brown
        case "Aqua":
            return UIColor(red: 0, green: 255, blue: 255, alpha: 1)
        case "Lime":
            return UIColor(red: 0, green: 255, blue: 0, alpha: 1)
        case "Gold":
            return UIColor(red: 255, green: 215, blue: 0, alpha: 1)
        default:
            return SKColor.black
        }
    }
    
    func getRgb(color: String) -> String {
        print("trying to get rgb: \(color)")
        switch color {
        case "Midnight":
            return "0,0,0,1"
        case "Black":
            return "1,1,1,1"
        case "Orange":
            return "255,51,51,1"
        case "Pink":
            return "255,20,147,1"
        case "Blue":
            return "0,0,255,1"
        case "Yellow":
            return "255,255,0,1"
        case "Default":
            return "0,0,0,0"
        case "Purple":
            return "128,0,128,1"
        case "Red":
            return "255,0,0,1"
        case "Green":
            return "0,128,0,1"
        case "Brown":
            return "165,42,42,1"
        case "Aqua":
            return "0,255,255,1"
        case "Lime":
            return "0,255,0,1"
        case "Gold":
            return "255,215,0,1"
        case "Ghost":
            return "-1,-1,-1,0"
        default:
            return "0,0,0,0"
        }
    }
    
    // set color to dino
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("setting color: \(colorItems[indexPath.row].itemName)")
        playSound(forResource: "button", ofType: "mp3")
        if(colorItems[indexPath.row].itemName != "Default"){
            dino.colorBlendFactor = 1
        } else {
            dino.colorBlendFactor = 0
        }
        dino.color = getColor(color: colorItems[indexPath.row].itemName)
        let docRef3 = db.collection("Users").document(user.uid)
        docRef3.updateData(["color" : self.getRgb(color: self.colorItems[indexPath.row].itemName)])
    }
    
    @objc func updateDino() {
        dinoImage.startAnimating()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.nightLabel.isHidden = true
        self.nightButton.isHidden = true
        var isNight = false
        scrollView.backgroundColor = .clear
        
        // get data from inventory table
        let docRef = db.collection("Inventory").whereField("userId", isEqualTo: user.uid)
        docRef.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error gettng documents \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if document.data()["category"] as! String == "avatars" {
                        self.nightLabel.isHidden = false
                        self.nightButton.isHidden = false
                        isNight = true
                        // restructure buttons
                    }
                    if document.data()["category"] as! String == "colors" {
                        let item = StoreItem(itemName: document.data()["name"] as! String, itemImage: "", itemPrice: 0, isSold: true, itemCategory: document.data()["category"] as! String)
                        
                        self.colorItems.append(item)
                        self.pages = Int(ceil(Double(self.colorItems.count) / 3.0))
                        self.pageControl.numberOfPages = self.pages
                        self.setupScreen()
                        self.collectionView.reloadData()
                    }
                }
                if !isNight {
                    self.desertLabel.frame.origin.x = self.view.frame.midX - self.desertLabel.frame.width/2
                    self.desertButton.frame.origin.x = self.view.frame.midX - self.desertButton.frame.width/2
                }
            }
        }
        // get data from users table
        let docRef2 = db.collection("Users").document(user.uid)
        docRef2.getDocument { (document, error) in
            if let document = document, document.exists {
                let darkMode = document.data()!["darkMode"] as! Bool
                if darkMode {
                    self.view.backgroundColor = UIColor.lightGray
                } else {
                    self.view.backgroundColor = UIColor.white
                }
                // color
                let arr = (document.data()!["color"] as! String).components(separatedBy: ",")
                if(arr[3] == "0") {
                    if arr[0] == "-1" {
                        // ghost mode
                        self.dino.color = SKColor.clear
                    } else {
                        self.dino.colorBlendFactor = 0
                    }
                }
                if arr[0] != "-1" {
                    self.dino.color = SKColor(red: CGFloat(Float(Int(arr[0])!)/255), green: CGFloat(Float(Int(arr[1])!)/255), blue: CGFloat(Float(Int(arr[2])!)/255), alpha: CGFloat(Float(Int(arr[3])!)))
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dinoAnimatedAtlas = SKTextureAtlas(named: "blinking")
        var dinoFrames: [SKTexture] = []
        let numImages = dinoAnimatedAtlas.textureNames.count
        for i in 1...numImages {
            let dinoTextureName = "blinking\(i)"
            dinoFrames.append(dinoAnimatedAtlas.textureNamed(dinoTextureName))
        }
        let firstFrameTexture = dinoFrames[0]
        dino.texture = firstFrameTexture
        
//        dino.texture = SKTexture(imageNamed: "dino")
        
//        dino.colorBlendFactor = 1
//        dino.color = SKColor.blue
        dino.position.x = skView.frame.width/2
        dino.position.y = skView.frame.height/2
        dino.size.height = skView.frame.size.height
        dino.size.width = skView.frame.size.width
        dino.colorBlendFactor = 1
//        let docRef3 = db.collection("PlayerProperties").document(user.uid)
//        docRef3.getDocument { (document, error) in
//            if let document = document, document.exists {
//                if document.data()!["level"] as! String == "night" {
//                    self.nightLabel.isHidden = false
//                    self.nightButton.isHidden = false
//                    // restructure buttons
//                }
//                let arr = (document.data()!["color"] as! String).components(separatedBy: ",")
//                if(arr[3] == "0") {
//                    if arr[0] == "-1" {
//                        // ghost mode
//                        self.dino.color = SKColor.clear
//                    } else {
//                        self.dino.colorBlendFactor = 0
//                    }
//                }
//                if arr[0] != "-1" {
//                    self.dino.color = SKColor(red: CGFloat(Float(Int(arr[0])!)/255), green: CGFloat(Float(Int(arr[1])!)/255), blue: CGFloat(Float(Int(arr[2])!)/255), alpha: CGFloat(Float(Int(arr[3])!)))
//                }
//            } else {
//                self.dino.colorBlendFactor = 0
//                let data = [
//                    "color" : "Default",
//                    "level" : "desert"
//                ]
//                self.db.collection("PlayerProperties").document(self.user!.uid).setData(data)
//            }
//        }
        
        let scene = SKScene(size: skView.frame.size)
        scene.backgroundColor = .clear
        skView.backgroundColor = .clear
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .aspectFit
        scene.addChild(dino)
        skView.presentScene(scene)
        let act1 = SKAction.animate(with: dinoFrames, timePerFrame: 0.3,resize:false,restore:true)
        let act2 = SKAction.wait(forDuration: 5)
        let seq = SKAction.sequence([act1,act2])
        dino.run(SKAction.repeatForever(seq), withKey: "dinoRunning")
        
//        dinoImage.animationImages = [UIImage(named: "blinking1")!, UIImage(named: "blinking2")!, UIImage(named: "blinking3")!]
//        dinoImage.animationRepeatCount = 1
//        dinoImage.animationDuration = 0.5
        
//        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateDino), userInfo: nil, repeats: true)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        scrollView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func setupScreen() {
//        for index in 0...(pages-1) {
//            frame.origin.x = scrollView.frame.size.width*CGFloat(index)
//            frame.size = scrollView.frame.size
//        }
        collectionView.backgroundColor = .clear
        scrollView.backgroundColor = .clear
        scrollView.contentSize = CGSize(width: (scrollView.frame.size.width*CGFloat(pages)), height: scrollView.frame.size.height)
        collectionView.contentSize = CGSize(width: (scrollView.frame.size.width*CGFloat(pages)), height: scrollView.frame.size.height)
//        scrollView.delegate = self as? UIScrollViewDelegate
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(pageNumber)
    }
    
    func playSound(forResource name: String, ofType ext: String) {
        
        // get path to the sound
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: name, ofType: ext)!)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: sound)
            audioPlayer?.play()
        } catch {
            print("Audio error")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
