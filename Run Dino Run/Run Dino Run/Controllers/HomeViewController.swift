//
//  HomeViewController.swift
//  Run Dino Run
//
//  Created by Jianbin Lin on 3/30/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import SpriteKit
import Firebase
import AVFoundation

class HomeViewController: UIViewController {

//    @IBOutlet weak var dinoImage: UIImageView!
    @IBOutlet weak var cloudOne: UIImageView!
    @IBOutlet weak var cloudTwo: UIImageView!
    @IBOutlet weak var cloudThree: UIImageView!
    @IBOutlet weak var cloudFour: UIImageView!
    @IBOutlet weak var cloudFive: UIImageView!
    @IBOutlet weak var cloudSiz: UIImageView!
    @IBOutlet weak var cloudSeven: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var storeButton: UIButton!
    @IBOutlet weak var customizeButton: UIButton!
    @IBOutlet weak var leaderboardButton: UIButton!
    @IBOutlet weak var palmTree: UIImageView!
    @IBOutlet weak var sun: UIImageView!
    @IBOutlet weak var skView: SKView!
    
    var user: User!
    var db: Firestore!
    var positions: [Float] = []
    var timer = Timer()
    var dino = SKSpriteNode()
    var darkMode = false
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playButton.titleLabel!.font = UIFont(name: "8BITWONDERNominal", size: 13)
        storeButton.titleLabel!.font = UIFont(name: "8BITWONDERNominal", size: 13)
        customizeButton.titleLabel!.font = UIFont(name: "8BITWONDERNominal", size: 13)
        leaderboardButton.titleLabel!.font = UIFont(name: "8BITWONDERNominal", size: 13)
        
        self.navigationController?.isNavigationBarHidden = false
        
        user = Auth.auth().currentUser
        db = Firestore.firestore()
        
        dino.position.x = skView.frame.width/2
        dino.position.y = skView.frame.height/2
        dino.size.height = skView.frame.size.height
        dino.size.width = skView.frame.size.width
        dino.colorBlendFactor = 0
        
        let scene = SKScene(size: skView.frame.size)
        scene.backgroundColor = .clear
        skView.backgroundColor = .clear
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .aspectFit
        scene.addChild(dino)
        skView.presentScene(scene)
//        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.updateDino), userInfo: nil, repeats: true)
        let dinoAnimatedAtlas = SKTextureAtlas(named: "blinking")
        var dinoFrames: [SKTexture] = []
        let numImages = dinoAnimatedAtlas.textureNames.count
        for i in 1...numImages {
            let dinoTextureName = "blinking\(i)"
            dinoFrames.append(dinoAnimatedAtlas.textureNamed(dinoTextureName))
        }
        let firstFrameTexture = dinoFrames[0]
        let act1 = SKAction.animate(with: dinoFrames, timePerFrame: 0.2,resize:false,restore:true)
        let act2 = SKAction.wait(forDuration: 5)
        let seq = SKAction.sequence([act1,act2])
        self.dino.run(SKAction.repeatForever(seq), withKey: "dinoRunning")
        
        dino.texture = firstFrameTexture
        positions.append(Float(cloudOne.frame.origin.x))
        positions.append(Float(cloudTwo.frame.origin.x))
        positions.append(Float(cloudThree.frame.origin.x))
        positions.append(Float(cloudFour.frame.origin.x))
        positions.append(Float(cloudFive.frame.origin.x))
        positions.append(Float(cloudSiz.frame.origin.x))
        positions.append(Float(cloudSeven.frame.origin.x))
    }
    
//    @objc func updateDino() {
//        dinoImage.startAnimating()
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cloudOne.frame.origin.x = CGFloat(positions[0])
        self.cloudTwo.frame.origin.x = CGFloat(positions[1])
        self.cloudThree.frame.origin.x = CGFloat(positions[2])
        self.cloudFour.frame.origin.x = CGFloat(positions[3])
        self.cloudFive.frame.origin.x = CGFloat(positions[4])
        self.cloudSiz.frame.origin.x = CGFloat(positions[5])
        self.cloudSeven.frame.origin.x = CGFloat(positions[6])
        
        let docRef = db.collection("Users").document(user.uid)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                self.darkMode = document.data()!["darkMode"] as! Bool
                if self.darkMode {
                    self.replaceWithDarkMode()
                } else {
                    self.replaceWithLightMode()
                }
                
                let arr = (document.data()!["color"] as! String).components(separatedBy: ",")

                self.dino.colorBlendFactor = 1
                if(arr[3] == "0") {
                    if arr[0] == "-1" {
                        // ghost mode
                        self.dino.color = SKColor.clear
                    } else {
                        self.dino.colorBlendFactor = 0
                    }
                }
                if arr[0] != "-1" {
                    self.dino.color = SKColor(red: CGFloat(Float(Int(arr[0])!)/255), green: CGFloat(Float(Int(arr[1])!)/255), blue: CGFloat(Float(Int(arr[2])!)/255), alpha: CGFloat(Float(Int(arr[3])!)))
                }
                
                UIView.animate (
                    withDuration: 2.0,
                    delay: 0.0,
                    options: .curveEaseOut,
                    animations: {
                        self.skView.frame.origin.x = self.view.frame.midX-(self.skView.frame.width/2)
                    }
                )
            }  else {
                   self.dino.colorBlendFactor = 0
                   let data = [
                       "color" : "0,0,0,0",
                       "level" : "desert",
                       "theme" : "light"
                   ]
                   self.db.collection("PlayerProperties").document(self.user!.uid).setData(data)
               }
        }
//        dinoImage.animationImages = [UIImage(named: "blinking1")!,UIImage(named: "blinking2")!, UIImage(named: "blinking3")!]
//        dinoImage.animationRepeatCount = 1
//        dinoImage.animationDuration = 0.5
        
        UIView.animate (
            withDuration: 90.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.cloudOne.frame.origin.x = self.view.frame.width
            }
        )
        UIView.animate (
            withDuration: 30.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.cloudTwo.frame.origin.x = self.view.frame.width
            }
        )
        UIView.animate (
            withDuration: 54.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.cloudThree.frame.origin.x = self.view.frame.width
            }
        )
        UIView.animate (
            withDuration: 75.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.cloudFour.frame.origin.x = self.view.frame.width
            }
        )
        UIView.animate (
            withDuration: 90.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.cloudFive.frame.origin.x = self.view.frame.width
            }
        )
        UIView.animate (
            withDuration: 110.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.cloudSiz.frame.origin.x = self.view.frame.width
            }
        )
        UIView.animate (
            withDuration: 110.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.cloudSeven.frame.origin.x = self.view.frame.width
            }
        )
    }
    
    func replaceWithDarkMode() {
        palmTree.image = UIImage(named: "palmTreeDark")
        sun.image = UIImage(named: "nightMoon.png")
        view.backgroundColor = SKColor.lightGray
        
        cloudOne.image = UIImage(named: "cloudDark")
        cloudTwo.image = UIImage(named: "cloudDark")
        cloudThree.image = UIImage(named: "cloudDark")
        cloudFour.image = UIImage(named: "cloudDark")
        cloudFive.image = UIImage(named: "cloudDark")
        cloudSiz.image = UIImage(named: "cloudDark")
        cloudSeven.image = UIImage(named: "cloudDark")
    }
    
    func replaceWithLightMode() {
        palmTree.image = UIImage(named: "palm1")
        sun.image = UIImage(named: "sun.png")
        view.backgroundColor = SKColor.white
        
        cloudOne.image = UIImage(named: "cloud")
        cloudTwo.image = UIImage(named: "cloud")
        cloudThree.image = UIImage(named: "cloud")
        cloudFour.image = UIImage(named: "cloud")
        cloudFive.image = UIImage(named: "cloud")
        cloudSiz.image = UIImage(named: "cloud")
        cloudSeven.image = UIImage(named: "cloud")
    }
    
    func playSound(forResource name: String, ofType ext: String) {
        
        // get path to the sound
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: name, ofType: ext)!)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: sound)
            audioPlayer?.play()
        } catch {
            print("Audio error")
        }
    }

    @IBAction func playBtn(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
    }
    
    @IBAction func storeBtn(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
    }
    
    @IBAction func customizeBtn(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
    }
    
    @IBAction func leaderboardBtn(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
