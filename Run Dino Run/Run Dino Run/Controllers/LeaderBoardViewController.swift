//
//  LeaderBoardViewController.swift
//  Run Dino Run
//
//  Created by Jianbin Lin on 3/30/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import Firebase

let customViewCellIdentifier = "tableID"
var score:[Player] = []

class LeaderBoardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var db: Firestore!
    var user: User! = Auth.auth().currentUser
    var darkMode = false
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        tableView.delegate = self
        tableView.dataSource = self
        db = Firestore.firestore()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        score = []
           let docRef = db.collection("Users").order(by: "bestScore", descending: true)
           
           docRef.getDocuments() { (querySnapshot, err) in
               if let err = err {
                   print("Error gettng documents \(err)")
               } else {
                   for document in querySnapshot!.documents {
                       let newScore = document.data()["bestScore"] as! Int
                       let userName = document.data()["username"] as! String
                       let level = document.data()["level"] as! String
                       let newPlayer = Player(u: userName, l: level, bs: newScore)
                       score.append(newPlayer)
                   }
                self.tableView.reloadData()
               }
           }
        
        let docRef2 = db.collection("Users").document(user.uid)
        docRef2.getDocument { (document, error) in
            if let document = document, document.exists {
                self.darkMode = document.data()!["darkMode"] as! Bool
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return score.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: customViewCellIdentifier,
                                                 for: indexPath) as! CustomViewCell
        let userName = score[indexPath.row].getUserName()
        cell.userName?.text = score[indexPath.row].getUserName()
        cell.userName.font = UIFont(name: "8BITWONDERNominal", size: 15)
        cell.rank?.text = "Rank: " + String(indexPath.row + 1)
        cell.rank.font = UIFont(name: "8BITWONDERNominal", size: 12)
        cell.score?.text = String(score[indexPath.row].getBestScore())
        cell.score.font = UIFont(name: "8BITWONDERNominal", size: 15)
        cell.level?.text = score[indexPath.row].getLevel()
        cell.level.font = UIFont(name: "8BITWONDERNominal", size: 15)
 
        if darkMode {
            cell.backgroundColor = .lightGray
            tableView.backgroundColor = .lightGray
            if userName == user.displayName {
                cell.backgroundColor = .white
            }
        }
        else {
            cell.backgroundColor = .white
            tableView.backgroundColor = .white
            if userName == user.displayName {
                cell.backgroundColor = .lightGray
            }
        }
//        else if userName == user.displayName {
//            cell.backgroundColor = .lightGray
//        }
        
        if indexPath.row >= 3 {
            cell.star.isHidden = true
        }
        
        return cell
    }

}
