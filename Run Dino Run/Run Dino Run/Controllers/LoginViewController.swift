//
//  LoginViewController.swift
//  Run Dino Run
//
//  Created by Jianbin Lin on 3/30/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var dinoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        // auto login
        DispatchQueue.main.async {
            self.autoLogin()
        }
        dinoImage.layer.magnificationFilter = CALayerContentsFilter(rawValue: kCISamplerFilterNearest)
        dinoImage.animationImages = [UIImage(named: "running1")!,UIImage(named: "running2")!]
        dinoImage.animationDuration = 0.3
        dinoImage.startAnimating()
        UIView.animate (
            withDuration: 1.0,
            delay: 0.0,
            options: .curveEaseOut,
            animations: {
                self.dinoImage.frame.origin.x += self.dinoImage.frame.size.width + 70
            },
            completion: { finished in
                self.dinoImage.stopAnimating()
                self.dinoImage.image = UIImage(named: "confusedDino")
                self.dinoImage.frame = CGRect(x: 95, y: 565, width: 225, height: 220)
            }
        )
        
    }
    
    func autoLogin() {
        
        guard let email = UserDefaults.standard.value(forKey: "email") else {
            return
        }
        guard let password = UserDefaults.standard.value(forKey: "password") else {
            return
        }
        
        self.dinoImage.image = UIImage(named: "dino")
        self.dinoImage.frame = CGRect(x: 95, y: 605, width: 225, height: 185)
        
        UIView.animate (
            withDuration: 1.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.dinoImage.frame.origin.x += self.dinoImage.frame.size.width + 70
            },
            completion: { finished in
                Utility.signIn(from: self, withEmail: email as! String,
                password: password as! String,
                firstLogIn: false,
                dinoImage: self.dinoImage)
            }
        )
    }
    
    func validateFields(_ email: String, _ password: String) -> String? {
        
        // check all fields are filled in
        if email == "" || password == "" {
            return "Please fill in all fields!"
        }
        return nil
    }
    
    @IBAction func logInBtn(_ sender: Any) {
        
        // user input
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        // input checking
        let error = validateFields(email, password)
        if error != nil {
            Utility.createAlert(on: self, "Error", error!)
            return
        }
        // sign in
        Utility.signIn(from: self, withEmail: email, password: password, firstLogIn: true, dinoImage: self.dinoImage)
    }
    
    
    

}
