//
//  ResetPasswordViewController.swift
//  Run Dino Run
//
//  Created by JianBin Lin on 5/5/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import FirebaseAuth
import AVFoundation

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var originalPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    let user = Auth.auth().currentUser!
    var audioPlayer: AVAudioPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func validateFields(_ originalPassword: String, _ newPassword: String, _ confirmPassword: String) -> String? {
        
        // check all fields are filled in
        if originalPassword == "" || newPassword == "" || confirmPassword == "" {
            return "Please fill in all fields!"
        }
        if newPassword != confirmPassword {
            return "Confirm Password not match!"
        }
        return nil
    }
    
    @IBAction func resetPassword(_ sender: Any) {
        
        playSound(forResource: "button", ofType: "mp3")
        
        let originalPassword = originalPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let newPassword = newPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let confirmPassword = confirmPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        // validate all fields
        let error = validateFields(originalPassword, newPassword, confirmPassword)
        if error != nil {
            Utility.createAlert(on: self, "Error", error!)
            return
        }
        // check original password
        Auth.auth().signIn(withEmail: user.email!, password: originalPassword) { (result, error) in
            // sign in fail
            if error != nil {
                Utility.createAlert(on: self, "Error", "Wrong Password!")
                return
            }
        }
        // reset password
        user.updatePassword(to: newPassword) { (error) in
            // reset fail
            if error != nil {
                Utility.createAlert(on: self, "Error", error!.localizedDescription)
                return
            }
            // reset success
            Utility.createAlert(on: self, "Sccuess", "You have reset password!")
            UserDefaults.standard.set(newPassword, forKey: "password")
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func playSound(forResource name: String, ofType ext: String) {
        
        // get path to the sound
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: name, ofType: ext)!)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: sound)
            audioPlayer?.play()
        } catch {
            print("Audio error")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
