//
//  SecondLaunchScreenViewController.swift
//  Run Dino Run
//
//  Created by XCodeClub on 2020-04-15.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import QuartzCore

class SecondLaunchScreenViewController: UIViewController {
    @IBOutlet weak var bigDinoImage: UIImageView!
    @IBOutlet weak var dinoImage: UIImageView!
    @IBOutlet weak var exclaimPointImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.exclaimPointImage.isHidden = true
        self.dinoImage.layer.magnificationFilter = CALayerContentsFilter(rawValue: kCISamplerFilterNearest)
        self.dinoImage.animationImages = [UIImage(named: "running1")!, UIImage(named: "running2")!]
        self.dinoImage.animationDuration = 0.3
        
        UIView.animate (
            withDuration: 1.0,
            delay: 0.0,
            options: .curveEaseIn,
            animations: {
                self.bigDinoImage.frame.origin.x += 58
                sleep(1)
                self.exclaimPointImage.isHidden = false
            },
            completion: { finished in
                UIView.animate (
                    withDuration: 1.0,
                    delay: 0.0,
                    options: .curveEaseIn,
                    animations: {
                        self.exclaimPointImage.isHidden = true
                        self.dinoImage.image = UIImage(named: "dino")
                        self.dinoImage.startAnimating()
                        self.dinoImage.frame.origin.x += self.dinoImage.frame.size.width + 80
                    },
                    completion: { finished in
                        let mainSB = UIStoryboard(name: "Main", bundle: nil)
                        let vc = mainSB.instantiateInitialViewController()
                        UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                )
            }
        )
    }
}
