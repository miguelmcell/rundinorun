//
//  SettingViewController.swift
//  Run Dino Run
//
//  Created by Jianbin Lin on 3/30/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import AVFoundation

class SettingViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    
    let user = Auth.auth().currentUser!
    var db: Firestore! = Firestore.firestore()
    var darkMode = false
    var audioPlayer: AVAudioPlayer?
    
    @IBOutlet weak var darkModeLabel: UILabel!
    @IBOutlet weak var darkModeSwitch: UISwitch!
    @IBOutlet weak var nameTextLabel: UILabel!
    
    @IBAction func darkModeSwitchOn(_ sender: Any) {
        if (self.darkModeSwitch.isOn) {
            self.darkMode = true
            self.view.backgroundColor = UIColor.lightGray
        } else {
            self.darkMode = false
            self.view.backgroundColor = UIColor.white
        }
        let docRef = db.collection("Users").document(user.uid)
        docRef.updateData(["darkMode": self.darkMode])
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // font
        nameTextLabel.font = UIFont(name: "8BITWONDERNominal", size: 15)
        usernameLabel.font = UIFont(name: "8BITWONDERNominal", size: 15)
        darkModeLabel.font = UIFont(name: "8BITWONDERNominal", size: 15)
        
        usernameLabel.text = user.displayName
        
        // dark mode
        let docRef = db.collection("Users").document(user.uid)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                self.darkMode = document.data()!["darkMode"] as! Bool
                if self.darkMode {
                    self.darkModeSwitch.isOn = true
                    self.view.backgroundColor = UIColor.lightGray
                } else {
                    self.darkModeSwitch.isOn = false
                    self.view.backgroundColor = UIColor.white
                }
            }
        }
    }
    
    func validateFields(_ originalPassword: String, _ newPassword: String, _ confirmPassword: String) -> String? {
        
        // check all fields are filled in
        if originalPassword == "" || newPassword == "" || confirmPassword == "" {
            return "Please fill in all fields!"
        }
        if newPassword != confirmPassword {
            return "Confirm Password not match!"
        }
        return nil
    }
    
    @IBAction func resetPassword(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
    }
    
    @IBAction func logOutBtn(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
        showActionSheet(title: "Log out", handler: logOutHandler)
    }
    
    func showActionSheet(title: String, handler: ((UIAlertAction) -> Void)?) {
        // create alert
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        // create button
        alert.addAction(UIAlertAction(title: title, style: UIAlertAction.Style.default, handler: handler))
        
        // Cancel button
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func logOutHandler(_ action: UIAlertAction) {
        // sign out
        do {
            try Auth.auth().signOut()
            self.logOutComplete()
        } catch let error {
            Utility.createAlert(on: self, "Error", error.localizedDescription)
        }
    }
    
    func logOutComplete() -> UINavigationController{
        
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "password")
        
        // go to Login screen
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "StartNavVC") as! UINavigationController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
        
        return nextViewController
    }
    
    @IBAction func deleteAccount(_ sender: Any) {
        playSound(forResource: "button", ofType: "mp3")
        showActionSheet(title: "Delete Account", handler: deleteAccountHandler)
    }
    
    func deleteAccountHandler(_ action: UIAlertAction) {
        user.delete { error in
            if let error = error {
                // An error happened.
                Utility.createAlert(on: self, "Error", error.localizedDescription)
                return
            }
            // remove user's data from firestore
            self.db.collection("Users").document(self.user.uid).delete() { error in
                if let error = error {
                    Utility.createAlert(on: self, "Error", error.localizedDescription)
                    return
                }
                // Account deleted.
                Utility.createAlert(on: self.logOutComplete(), "Success", "You have deleted your account!")
            }
        }
    }
    
    func playSound(forResource name: String, ofType ext: String) {
        
        // get path to the sound
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: name, ofType: ext)!)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: sound)
            audioPlayer?.play()
        } catch {
            print("Audio error")
        }
    }
    
}
