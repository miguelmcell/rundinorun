//
//  SignUpViewController.swift
//  Run Dino Run
//
//  Created by Jianbin Lin on 3/30/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import QuartzCore

class SignUpViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var dinoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        // dino animation
        dinoImage.layer.magnificationFilter = CALayerContentsFilter(rawValue: kCISamplerFilterNearest)
        dinoImage.animationImages = [UIImage(named: "running1")!,UIImage(named: "running2")!]
        dinoImage.animationDuration = 0.3
        dinoImage.startAnimating()
        
        UIView.animate (
            withDuration: 1.0,
            delay: 0.0,
            options: .curveLinear,
            animations: {
                self.dinoImage.frame.origin.x += self.dinoImage.frame.size.width + 70
            },
            completion: { [weak self] finished in
                self!.dinoImage.stopAnimating()
            }
        )
    }
    
    func validateFields(_ username: String, _ email: String, _ password: String, _ confirmPassword: String) -> String? {
        
        // check all fields are filled in
        if username == "" || email == "" || password == "" || confirmPassword == "" {
            return "Please fill in all fields!"
        }
        if password != confirmPassword {
            return "Confirm Password not match!"
        }
        return nil
    }
    
    @IBAction func signUpBtn(_ sender: Any) {
        
        // user input
        let username = usernameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let confirmPassword = confirmPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        // input checking
        let error = validateFields(username, email, password, confirmPassword)
        if error != nil {
            Utility.createAlert(on: self, "Error", error!)
            return
        }
        // create user
        signUp(username: username, email: email, password: password)
    }
    
    func signUp(username: String, email: String, password: String) {
       
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            
            // create user failed
            if error != nil {
                Utility.createAlert(on: self, "Error", error!.localizedDescription)
                return
            }
            // update user's display name
            let changeRequest = result?.user.createProfileChangeRequest()
            changeRequest?.displayName = username
            changeRequest?.commitChanges { (error) in
              if error != nil {
                  Utility.createAlert(on: self, "Error", error!.localizedDescription)
                  return
              }
            }
            // dino animation
            DispatchQueue.main.async {
                self.dinoImage.startAnimating()
                UIView.animate (
                    withDuration: 1.0,
                    delay: 0.0,
                    options: .curveEaseOut,
                    animations: {
                        self.dinoImage.frame.origin.x += self.dinoImage.frame.size.width
                    }
                )
            }
            // store user's info to the firestore
            let db = Firestore.firestore()
            db.collection("Users").document(result!.user.uid).setData([
                "username": username,
                "email": email,
                "uid": result!.user.uid,
                "money": 0,
                "bestScore": 0,
                "color": "0,0,0,0",
                "level": "desert",
                "darkMode" : false]) { (error) in
                    
                    // store user's info failed
                    if error != nil {
                        Utility.createAlert(on: self, "Error", error!.localizedDescription)
                        return
                    }
            }
            
            // sign in
            Utility.createAlert(on: self, "Success", "Create user successfully")
            Utility.signIn(from: self, withEmail: email, password: password, firstLogIn: true, dinoImage: self.dinoImage)
        
        }
    }

}
