//
//  StoreViewController.swift
//  Run Dino Run
//
//  Created by Jianbin Lin on 3/30/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import Firebase
import SpriteKit
import AVFoundation

// for now items are dependent on index, so dont change order of the store items list
// index = index position in store, can fetch info about item in there
protocol InventoryDelegator {
    func addItem(index:Int,name:String, category:String)
}

class StoreViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, InventoryDelegator {
    
    @IBOutlet weak var categoryOutlet: UISegmentedControl!
    @IBOutlet weak var purchaseButtonOutlet: UIButton!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var dinoPreview: UIView!
    @IBOutlet weak var debugLabel: UILabel!
    @IBOutlet weak var catalogScrollView: UIScrollView!
    @IBOutlet weak var catalogPageControll: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var pages:Int = 0
    var frame = CGRect.zero
    var ammount = 0
    var purchasedItems: [PurchasedItem] = []
    var storeItems:[StoreItem] = [] // currentStoreItems
    var allItems:[StoreItem] = [] // items from all catogories
    var boughtItems:[StoreItem] = []
    var curSelectedItem:Int = -1
    var curSelectedCategory:String = "colors"
    var audioPlayer: AVAudioPlayer?
    let category1Name:String = "avatars"
    let category2Name:String = "accessories"
    let category3Name:String = "colors"
    let user = Auth.auth().currentUser!
    let db = Firestore.firestore()
    
    // GO WITH NAME!!!
    func isItemSold(name: String) -> Bool {
        // heed to look for name
//        print("can we find the item \(item.name!)")
        for item in purchasedItems {
            if (item.name == name) {
                return true
            }
        }
        return false
    }
    
    func searchItemFromList(name: String) -> Int {
        for (index, item) in allItems.enumerated() {
            if item.itemName == name {
                return index
            }
        }
        return -1
    }

    func addItem(index: Int, name: String, category: String) {
        let data = [
            "storeIndex": index,
            "name": name,
            "category": category,
            "userId": user.uid
            ] as [String : Any]
        db.collection("Inventory").addDocument(data: data)
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let newItem = PurchasedItem(context: managedContext)
        newItem.storeIndex = Int64(index)
        newItem.name = name
        newItem.category = category
        purchasedItems.append(newItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storeItems.count
    }
    
    func switchItemDisplay(categoryName: String) {
        storeItems = []
        for item in allItems {
            if(item.itemCategory == categoryName) {
                storeItems.append(item)
            }
        }
        pages = Int(ceil(Double(storeItems.count) / 9.0))
        catalogPageControll.numberOfPages = pages
        catalogScrollView.backgroundColor = .clear
        catalogScrollView.contentSize = CGSize(width: (catalogScrollView.frame.size.width*CGFloat(pages)), height: catalogScrollView.frame.size.height)
        collectionView.reloadData()
    }
    
    // GET TOTAL DISPLAY ITEM NUMBER AND ADD IT TO SWITCH ITEM FUNCITON TOO AS PAGE NUMBERS CHANGE PER SEGMENT
    func getTotalDisplayItems() -> Int {
        storeItems = []
        var count = 0
        for item in allItems {
            if(item.itemCategory == curSelectedCategory) {
                count += 1
            }
        }
        return count
    }

    @IBAction func storeCategorySegmentControl(_ sender: Any) {
        switch categoryOutlet.selectedSegmentIndex
        {
        case 0:
            previewOff()
            curSelectedCategory = category3Name
            [catalogScrollView .setContentOffset(CGPoint.zero, animated: true)]
            catalogPageControll.currentPage = 0
            switchItemDisplay(categoryName: category3Name)
        case 1:
            previewOff()
            curSelectedCategory = category1Name
            [catalogScrollView .setContentOffset(CGPoint.zero, animated: true)]
            catalogPageControll.currentPage = 0
            switchItemDisplay(categoryName: category1Name)
            
        default:
            break
        }
    }
    
    @IBAction func purchaseButton(_ sender: Any) {
        
        playSound(forResource: "button", ofType: "mp3")
        
        // check if enough money, then subtract money
        if(storeItems[curSelectedItem].itemPrice > ammount){
            debugLabel.text = "Not enough money to purchase"
            return
        }
        // update money
        db.collection("Users").document(user.uid).updateData(["money": ammount - storeItems[curSelectedItem].itemPrice])
        
        moneyLabel.text = String((ammount - storeItems[curSelectedItem].itemPrice))
        
        previewOff()
        let indexPath = IndexPath(item: curSelectedItem, section: 0)
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
            (self.collectionView!.cellForItem(at: indexPath) as! StoreItemCollectionViewCell).itemSoldImage.alpha = 1.0
        }, completion: {(finished: Bool) in
            
        })
        
        let storeCategory:String = curSelectedCategory
        addItem(index: curSelectedItem, name: storeItems[curSelectedItem].itemName, category: storeCategory)
        debugLabel.text = "Added \(storeItems[curSelectedItem].itemName) to inventory"
    }
    
    // green brown aqua lime gold ghost
    func getColor(color: String) -> SKColor {
        switch color {
        case "Midnight":
            return SKColor.black
        case "Orange":
            return SKColor.orange
        case "Pink":
            return SKColor.systemPink
        case "Blue":
            return SKColor.blue
        case "Yellow":
            return SKColor.yellow
        case "Ghost":
            return SKColor.clear
        case "Default":
            return SKColor.gray
        case "Purple":
            return SKColor.purple
        case "Red":
            return SKColor.red
        case "Green":
            return SKColor.green
        case "Brown":
            return SKColor.brown
        case "Aqua":
            return UIColor(red: 0, green: 255, blue: 255, alpha: 1)
        case "Lime":
            return UIColor(red: 0, green: 255, blue: 0, alpha: 1)
        case "Gold":
            return UIColor(red: 255, green: 215, blue: 0, alpha: 1)
        default:
            return SKColor.black
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreItemCell", for: indexPath) as! StoreItemCollectionViewCell
        let row = indexPath.row
        cell.itemSoldImage.alpha = 0
        if(isItemSold(name: storeItems[row].itemName)) {
            cell.itemSoldImage.alpha = 1
        }
        cell.itemNameLabel.text = storeItems[row].itemName
        cell.itemImage.image = nil
        cell.itemImage.backgroundColor = SKColor.clear
        if storeItems[row].itemImage != "COLOR" {
            cell.itemImage.image = UIImage(named: storeItems[row].itemImage)
        }
        else {
            cell.itemImage.backgroundColor = getColor(color: storeItems[row].itemName)
        }
        cell.itemPriceLabel.text = String(storeItems[row].itemPrice)
        return cell
    }
    
    // select item
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        playSound(forResource: "button", ofType: "mp3")
        
        if ((collectionView.cellForItem(at: indexPath) as! StoreItemCollectionViewCell).itemSoldImage.alpha == 1) {
            debugLabel.text = "Item already purchased"
            return
        }
        debugLabel.text = "Selected \(storeItems[indexPath.row].itemName) for \(storeItems[indexPath.row].itemPrice) coins"
        curSelectedItem = indexPath.row
        previewOn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        switchItemDisplay(categoryName: category3Name)
    }
    override func viewDidAppear(_ animated: Bool) {
        let docRef2 = db.collection("Users").document(user.uid)
        docRef2.getDocument { (document, error) in
            if let document = document, document.exists {
                let darkMode = document.data()!["darkMode"] as! Bool
                if darkMode {
                    self.view.backgroundColor = UIColor.lightGray
                } else {
                    self.view.backgroundColor = UIColor.white
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear

        // Do any additional setup after loading the view.
        allItems.append(StoreItem(itemName: "Night", itemImage: "cresMoon.jpeg", itemPrice: 200, isSold: false, itemCategory: category1Name))
        allItems.append(StoreItem(itemName: "Blue", itemImage: "COLOR", itemPrice: 40, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Black", itemImage: "COLOR", itemPrice: 40, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Brown", itemImage: "COLOR", itemPrice: 40, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Purple", itemImage: "COLOR", itemPrice: 80, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Pink", itemImage: "COLOR", itemPrice: 80, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Yellow", itemImage: "COLOR", itemPrice: 80, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Orange", itemImage: "COLOR", itemPrice: 80, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Red", itemImage: "COLOR", itemPrice: 100, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Green", itemImage: "COLOR", itemPrice: 100, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Aqua", itemImage: "COLOR", itemPrice: 100, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Lime", itemImage: "COLOR", itemPrice: 100, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Gold", itemImage: "COLOR", itemPrice: 600, isSold: false, itemCategory: category3Name))
        allItems.append(StoreItem(itemName: "Ghost", itemImage: "COLOR", itemPrice: 300, isSold: false, itemCategory: category3Name))
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // get money
        let docRef = db.collection("Users").document(user.uid)
            docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                self.ammount = document.data()!["money"] as! Int
                self.moneyLabel.text = String(self.ammount)
            }
        }
        // get items
        let docRef2 = db.collection("Inventory").whereField("userId", isEqualTo: user.uid)
        docRef2.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error gettng documents \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let item = PurchasedItem(context: managedContext)
                    item.category = (document.data()["category"] as! String)
                    item.name = (document.data()["name"] as! String)
                    item.storeIndex = Int64(document.data()["storeIndex"] as! Int)
                    self.purchasedItems.append(item)
                    self.collectionView.reloadData()
                }
            }
        }
        
        pages = Int(ceil(Double(getTotalDisplayItems()) / 9.0))
        catalogPageControll.numberOfPages = pages
        
        setupScreens()
        purchaseButtonOutlet.isHidden = true
        catalogScrollView.delegate = self as? UIScrollViewDelegate
        
        dinoPreview.backgroundColor = .clear
        let xPosition = self.frame.width/4
        let yPosition = dinoPreview.frame.origin.y
        let width = dinoPreview.frame.size.width
        let height = dinoPreview.frame.size.height
        dinoPreview.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
    }
    
    func previewOn() {
        self.purchaseButtonOutlet.isHidden = false
        
        let xPosition = self.frame.width/15
        let yPosition = dinoPreview.frame.origin.y
        let width = dinoPreview.frame.size.width
        let height = dinoPreview.frame.size.height
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.purchaseButtonOutlet.alpha = 1.0
            self.dinoPreview.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
        }, completion: {(finished: Bool) in
//            print("finished")
        })
    }
    
    func previewOff() {
        let xPosition = self.frame.width/4
        let yPosition = dinoPreview.frame.origin.y
        let width = dinoPreview.frame.size.width
        let height = dinoPreview.frame.size.height
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.purchaseButtonOutlet.alpha = 0.0
            self.dinoPreview.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
        }, completion: {(finished: Bool) in
            self.purchaseButtonOutlet.isHidden = true
        })
    }
    
    func setupScreens() {
        for index in 0...(pages-1) {
            frame.origin.x = catalogScrollView.frame.size.width*CGFloat(index)
            frame.size = catalogScrollView.frame.size
        }
        catalogScrollView.contentSize = CGSize(width: (catalogScrollView.frame.size.width*CGFloat(pages)), height: catalogScrollView.frame.size.height)
        catalogScrollView.delegate = self as? UIScrollViewDelegate
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        catalogPageControll.currentPage = Int(pageNumber)
    }
    
    func deleteAllData(_ entity: String) {
        let regVar = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let delAllRegVar = NSBatchDeleteRequest(fetchRequest: regVar)
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        do {
            try managedContext.execute(delAllRegVar)}
        catch {print(error)}
    }
    
    func playSound(forResource name: String, ofType ext: String) {
        
        // get path to the sound
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: name, ofType: ext)!)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: sound)
            audioPlayer?.play()
        } catch {
            print("Audio error")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
