//
//  Cactus.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/9/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import SpriteKit
import GameplayKit

class Cactus: GKEntity {
    let collisionCategory: UInt32 = 0x1 << 1
    
    init(entityManager: EntityManager) {
        super.init()
        let texture = SKTexture(imageNamed: "cactus")
        let spriteComponent = SpriteComponent(texture: texture, collisionCategory: collisionCategory)
        addComponent(spriteComponent)
        addComponent(CactusComponent())
//        addComponent(MoveComponent(maxSpeed: 10, maxAcceleration: 5, radius: Float(texture.size().width * 0.3), entityManger: entityManager))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
