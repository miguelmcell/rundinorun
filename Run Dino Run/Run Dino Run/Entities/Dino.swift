//
//  Dino.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/9/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import SpriteKit
import GameplayKit

class Dino: GKEntity {
    let collisionCategory: UInt32 = 0x1 << 0
    
    init(imageName: String, entityManager: EntityManager) {
        super.init()
        
        let spriteComponent = SpriteComponent(texture: SKTexture(imageNamed: imageName), collisionCategory: collisionCategory)
        addComponent(spriteComponent)
        addComponent(DinoComponent())
//        addComponent(MoveComponent(maxSpeed: 0, maxAcceleration: 0, radius: Float(spriteComponent.node.size.width/2), entityManger: entityManager))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
