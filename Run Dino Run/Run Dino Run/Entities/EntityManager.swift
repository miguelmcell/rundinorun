//
//  EntityManager.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/9/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class EntityManager {
    lazy var componentSystems: [GKComponentSystem] = {
        let dinoSystem = GKComponentSystem(componentClass: DinoComponent.self)
        let moveSystem = GKComponentSystem(componentClass: MoveComponent.self)
        let cactusSystem = GKComponentSystem(componentClass: CactusComponent.self)
        return [dinoSystem, moveSystem, cactusSystem]
    }()
    
    var entities = Set<GKEntity>()
    var toRemove = Set<GKEntity>()
    let scene: SKScene
    let dinoCategory: UInt32 = 0x1 << 0
    let cactusCategory: UInt32 = 0x1 << 1
    
    init(scene: SKScene) {
        self.scene = scene
    }
    
    func add(_ entity: GKEntity) {
        entities.insert(entity)
        
        if let spriteNode = entity.component(ofType: SpriteComponent.self)?.node {
            scene.addChild(spriteNode)
        }
        for componentSystem in componentSystems {
            componentSystem.addComponent(foundIn: entity)
        }
    }
    
    func remove(_ entity: GKEntity) {
        if let spriteNode = entity.component(ofType: SpriteComponent.self)?.node {
            spriteNode.removeFromParent()
        }
        
        entities.remove(entity)
        toRemove.insert(entity)
    }
    
    func update(_ deltaTime: CFTimeInterval) {
        for componentSystem in componentSystems {
            componentSystem.update(deltaTime: deltaTime)
        }
        
        for currentRemove in toRemove {
            for componentSystem in componentSystems {
                componentSystem.removeComponent(foundIn: currentRemove)
            }
        }
        toRemove.removeAll()
    }
    
    func dino() -> GKEntity? {
        for entity in entities {
            if entity.component(ofType: DinoComponent.self) != nil {
                return entity
            }
        }
        return nil
    }
    
    func spawnCactus(x: Float, y: Float, speed: Float) {
        guard let dinoEntity = dino(),
            let _ = dinoEntity.component(ofType: DinoComponent.self),
            let _ = dinoEntity.component(ofType: SpriteComponent.self)
            else {
                return
        }
        
        let cactus = Cactus(entityManager: self)
        if let spriteComponent = cactus.component(ofType: SpriteComponent.self) {
            let offset = Int.random(in: 0 ... 40);
            spriteComponent.node.size = CGSize(width: 40+(offset/4), height: 80+offset)
            spriteComponent.node.position = CGPoint(x: CGFloat(x), y: CGFloat(y + Float(offset/2)))
            spriteComponent.node.zPosition = 2
            
            let distance = spriteComponent.node.position.x
//            print(distance)
//            print(distance/620)
            
            var baseTime = (distance/620)*5
            
            if(baseTime - CGFloat(speed) <= 1.5) {
                baseTime = baseTime - 2.5
            } else {
                baseTime -= CGFloat(speed)
            }
            let move = SKAction.moveTo(x: -spriteComponent.node.size.width, duration: TimeInterval(baseTime))
            
            let moveToSequence = SKAction.sequence([move])
            spriteComponent.node.run(moveToSequence, completion: {
                (self.scene as! GameScene).addScore()
                (self.scene as! GameScene).addSessionCoins(amount: 15)
                self.remove(cactus)
            })
        }
        
        add(cactus)
    }
    
    func entitiesFunc() -> [GKEntity] {
        return entities.compactMap{ entity in
            if entity.component(ofType: DinoComponent.self) != nil {
                return entity
            }
            return nil
        }
    }
    func cactusEntitiesFunc() -> [GKEntity] {
        return entities.compactMap{ entity in
            if entity.component(ofType: CactusComponent.self) != nil {
                return entity
            }
            return nil
        }
    }
    func pauseComponents() {
        let entitiesToPause = cactusEntitiesFunc()
        for entity in entitiesToPause {
            entity.component(ofType: SpriteComponent.self)?.node.isPaused = true
        }
    }
    func moveComponents() -> [MoveComponent] {
        let entitiesToMove = entitiesFunc()
        var moveComponents = [MoveComponent]()
        for entity in entitiesToMove {
            if let moveComponent = entity.component(ofType: MoveComponent.self) {
                moveComponents.append(moveComponent)
            }
        }
        return moveComponents
    }
}
