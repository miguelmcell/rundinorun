//
//  ButtonNode.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/8/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import Foundation
import SpriteKit

class ButtonNode: SKSpriteNode {
    let onButtonPress: () -> ()
    let onButtonEndPress: () -> ()
    
    init(size: CGSize, onButtonEndPress: @escaping () -> (), onButtonPress: @escaping () -> ()) {
        self.onButtonPress = onButtonPress
        self.onButtonEndPress = onButtonEndPress
        super.init(texture: nil, color: SKColor.clear, size: size)
        isUserInteractionEnabled = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        onButtonPress()
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        onButtonEndPress()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
