//
//  GameScene.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/8/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit
import Firebase
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let margin = CGFloat(10)
    let dinoCategory: UInt32 = 0x1 << 0
    let cactusCategory: UInt32 = 0x1 << 1
    
    // Buttons
    var controlsButton: ButtonNode!
    
    // Images
    var livesImage = SKSpriteNode(imageNamed: "egg3.png")
    
    // Labels
    var scoreLabel = SKLabelNode(fontNamed: "Courier-Bold")
    var scoreIndicator = SKLabelNode(fontNamed: "Courier-Bold")
    var slideUpDirectionLabel = SKLabelNode(fontNamed: "Courier")
    var holdDownDirectionLabel = SKLabelNode(fontNamed: "Courier")
    var moneyIndicator = SKLabelNode(fontNamed: "Courier-Bold")
    
    // Update time
    var lastUpdateTimeInterval: TimeInterval = 0
    var entityManager: EntityManager!
    var gameOver = false
    var dinoPlayer:Dino? = nil
    var isGrounded = true
    var score = 0
    var bestScore = 0
    var lives = 3
    var curMoney = 0
    var sessionCoins = 0
    var curAmmount = 0
    var prevAmmount = 0
    var timer = Timer()
    var coinTimer = Timer()
    var speedMult:Float = 0
    var totalDistance:Double = 0
    var spawnCooldown:Float = 5
    var label3 = SKLabelNode(fontNamed: "Courier-Bold")
    var background1 = SKSpriteNode(imageNamed: "realGround")
    var background2 = SKSpriteNode(imageNamed: "realGround")
    var moveLeft: SKAction = SKAction()
    var moveLeft2: SKAction = SKAction()
    var isAddingMoney = false
    var user: User! = Auth.auth().currentUser
    var db: Firestore! = Firestore.firestore()
    var audioPlayer: AVAudioPlayer!
    
    func didBegin(_ contact: SKPhysicsContact) {
        if (contact.bodyA.categoryBitMask == dinoCategory) && (contact.bodyB.categoryBitMask == cactusCategory) {
            loseLive()
        }
    }
    
    func spawnCactus() {
        entityManager.spawnCactus(x: Float(self.size.width),y: Float((self.size.height/2)+34), speed: speedMult)
    }
    
    @objc func speedUp() {
        speedMult += 0.5
        background1.speed += 0.5
        background2.speed += 0.5
    }
    
    override func didMove(to view: SKView) {
        print("scene size \(size)")
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.speedUp), userInfo: nil, repeats: true)
        coinTimer.invalidate()
        self.backgroundColor = .white
        self.physicsWorld.contactDelegate = self as SKPhysicsContactDelegate
        
        // Adding a temporary 'game view' background, will be replaced by an actual game view
        background1 = SKSpriteNode(imageNamed: "realGround")
        background1.anchorPoint = CGPoint.zero
        background1.position = CGPoint(x: background1.texture!.size().width * CGFloat(0) - CGFloat(1 * 0), y: (size.height/2))
        background1.zPosition = -1
        addChild(background1)
        moveLeft = SKAction.moveBy(x: -background1.texture!.size().width, y: 0, duration: 20)
        let moveReset = SKAction.moveBy(x: background1.texture!.size().width, y: 0, duration: 0)
        let moveLoop = SKAction.sequence([moveLeft, moveReset])
        let moveForever = SKAction.repeatForever(moveLoop)
        background1.run(moveForever)
        
        background2 = SKSpriteNode(imageNamed: "realGround")
        background2.anchorPoint = CGPoint.zero
        background2.position = CGPoint(x: background2.texture!.size().width * CGFloat(1) - CGFloat(1 * 1), y: (size.height/2))
        background2.zPosition = -1
        addChild(background2)
        moveLeft2 = SKAction.moveBy(x: -background2.texture!.size().width, y: 0, duration: 20)
        let moveReset2 = SKAction.moveBy(x: background2.texture!.size().width, y: 0, duration: 0)
        let moveLoop2 = SKAction.sequence([moveLeft2, moveReset2])
        let moveForever2 = SKAction.repeatForever(moveLoop2)
        background2.run(moveForever2)
        
        // Adding Dino line map beneath gameview/background
        let dinoLine = SKShapeNode()
        let pathToDraw = CGMutablePath()
        pathToDraw.move(to: CGPoint(x:margin,y:view.frame.midY-60))
        pathToDraw.addLine(to: CGPoint(x: size.width-margin, y: view.frame.midY-60))
        dinoLine.path = pathToDraw
        dinoLine.strokeColor = SKColor.black
        self.addChild(dinoLine)
        
        // Adding Score label
        scoreLabel.fontSize = 30
        scoreLabel.fontName = "8BITWONDERNominal"
        scoreLabel.fontColor = SKColor.black
        scoreLabel.position = CGPoint(x: size.width/2, y: (view.frame.height/2) + (view.frame.height/3))
        scoreLabel.zPosition = 1
        scoreLabel.horizontalAlignmentMode = .center
        scoreLabel.verticalAlignmentMode = .center
        scoreLabel.text = "Score"
        self.addChild(scoreLabel)
        
        // Adding score text indicator beneath score label
        scoreIndicator.fontSize = 25
//        scoreIndicator.fontName = "8BITWONDERNominal"
        scoreIndicator.fontColor = SKColor.gray
        scoreIndicator.position = CGPoint(x: size.width/2, y: scoreLabel.position.y-scoreLabel.frame.height-10)
        scoreIndicator.zPosition = 1
        scoreIndicator.horizontalAlignmentMode = .center
        scoreIndicator.verticalAlignmentMode = .center
        scoreIndicator.text = String(score)
        self.addChild(scoreIndicator)
        
        // Adding money text indicator
        moneyIndicator.fontSize = 25
//        moneyIndicator.fontName = "8BITWONDERNominal"
        moneyIndicator.fontColor = SKColor.darkGray
        moneyIndicator.position = CGPoint(x: size.width-25, y: scoreIndicator.position.y)
        moneyIndicator.zPosition = 1
        moneyIndicator.horizontalAlignmentMode = .right
        moneyIndicator.verticalAlignmentMode = .center
        moneyIndicator.text = "0¢"
        self.addChild(moneyIndicator)
        
        // Adding Egg lives
        
        livesImage.size = CGSize(width: 110, height: 60)
        livesImage.position = CGPoint(x: margin + livesImage.size.width/2, y: scoreLabel.position.y)
        self.addChild(livesImage)
        
        // Add controls button
        controlsButton = ButtonNode(size: CGSize(width: size.width, height: size.height), onButtonEndPress: buttonReleased ,onButtonPress: buttonPressed)
        controlsButton.position = CGPoint(x: size.width/2, y: size.height/2)
        
        let mainButton = UIButton()
        mainButton.frame.size.width = view.frame.width
        mainButton.frame.size.height = view.frame.height
        mainButton.frame.origin.x = 0
        mainButton.frame.origin.y = 0
        mainButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        let longPressGR = UILongPressGestureRecognizer(target: self, action: #selector(longPress))
        longPressGR.minimumPressDuration = 0.1
        mainButton.addGestureRecognizer(longPressGR)
        view.addSubview(mainButton)
        
//        self.addChild(controlsButton)
        
        // Slide Up to jump text
        slideUpDirectionLabel.fontSize = 12
        slideUpDirectionLabel.fontColor = SKColor.black
        slideUpDirectionLabel.position = CGPoint(x: size.width/2, y: (size.height/4))
        slideUpDirectionLabel.zPosition = 0
        slideUpDirectionLabel.horizontalAlignmentMode = .center
        slideUpDirectionLabel.verticalAlignmentMode = .center
        slideUpDirectionLabel.text = "Tap to Jump | Hold to Jump Higher"
        slideUpDirectionLabel.fontName = "8BITWONDERNominal"
        let waitAction = SKAction.wait(forDuration: 5)
        let fadeOut = SKAction.fadeAlpha(to: 0, duration: 1)
        let hideText = SKAction.run{
            self.slideUpDirectionLabel.isHidden = true
        }
        let action = SKAction.sequence([waitAction,fadeOut,hideText])
        slideUpDirectionLabel.run(action)
        self.addChild(slideUpDirectionLabel)
        
        entityManager = EntityManager(scene: self)
        
        dinoPlayer = Dino(imageName: "dino", entityManager: entityManager)
        
        if let spriteComponent = dinoPlayer!.component(ofType: SpriteComponent.self) {
            spriteComponent.node.size = CGSize(width: 60, height: 60)
            spriteComponent.node.position = CGPoint(x: spriteComponent.node.size.width/2+20, y: (size.height/2)+(spriteComponent.node.size.height/2)+8 )
            
            let dinoAnimatedAtlas = SKTextureAtlas(named: "Running")
            
            var dinoWalkingFrames: [SKTexture] = []
            let numImages = dinoAnimatedAtlas.textureNames.count
            for i in 1...numImages {
                let dinoTextureName = "running\(i)"
                dinoWalkingFrames.append(dinoAnimatedAtlas.textureNamed(dinoTextureName))
            }
            
            let firstFrameTexture = dinoWalkingFrames[0]
            spriteComponent.node.texture = firstFrameTexture
            spriteComponent.node.colorBlendFactor = 1
            
            let docRef = db.collection("Users").document(user.uid)
            docRef.getDocument { (document, error) in
                if let document = document, document.exists {
                    
                    print("checking player properties \(document.data()!["level"] as! String)")
                    
                    // level
                    if((document.data()!["level"] as! String) == "night") {
                        self.backgroundColor = SKColor.lightGray
                    }
                    // color
                    let arr = (document.data()!["color"] as! String).components(separatedBy: ",")
                    if(arr[3] == "0") {
                        if arr[0] == "-1" {
                            // ghost mode
                            spriteComponent.node.color = SKColor.clear
                        } else {
                            spriteComponent.node.colorBlendFactor = 0
                        }
                    }
                    if arr[0] != "-1" {
                        spriteComponent.node.color = SKColor(red: CGFloat(Float(Int(arr[0])!)/255), green: CGFloat(Float(Int(arr[1])!)/255), blue: CGFloat(Float(Int(arr[2])!)/255), alpha: CGFloat(Float(Int(arr[3])!)))
                    }
                    // money
                    self.curMoney = document.data()!["money"] as! Int
                    // best score
                    self.bestScore = document.data()!["bestScore"] as! Int
                }
            }
//            spriteComponent.node.color = PlayerSettings.shared().playerColor
            
            spriteComponent.node.run(SKAction.repeatForever(
                SKAction.animate(with: dinoWalkingFrames,
                                 timePerFrame: 0.1,
                                 resize: false,
                                 restore: true)),
                     withKey: "dinoRunning")
        }
        entityManager.add(dinoPlayer!)
        
    }
    
//    func restartGame() {
//        livesImage = SKSpriteNode(imageNamed: "egg3.png")
//        removeAllChildren()
//
//        // Labels
//        scoreLabel = SKLabelNode(fontNamed: "Courier-Bold")
//        scoreIndicator = SKLabelNode(fontNamed: "Courier-Bold")
//        slideUpDirectionLabel = SKLabelNode(fontNamed: "Courier")
//        holdDownDirectionLabel = SKLabelNode(fontNamed: "Courier")
//        moneyIndicator = SKLabelNode(fontNamed: "Courier-Bold")
//        lastUpdateTimeInterval = 0
//        entityManager = nil
//        gameOver = false
//        dinoPlayer = nil
//        isGrounded = true
//        score = 0
//        lives = 3
//        sessionCoins = 0
//        curAmmount = 0
//        prevAmmount = 0
//        timer = Timer()
//        coinTimer = Timer()
//        speedMult = 0
//        totalDistance = 0
//        spawnCooldown = 5
//        label3 = SKLabelNode(fontNamed: "Courier-Bold")
//        background1 = SKSpriteNode(imageNamed: "realGround")
//        background2 = SKSpriteNode(imageNamed: "realGround")
//        moveLeft = SKAction()
//        moveLeft2 = SKAction()
//        isAddingMoney = false
//
//        view?.reloadInputViews()
//        self.didMove(to: view!)
//    }
    
    @objc func tap(tapGestureRecognizer: UITapGestureRecognizer) {
        if (isGrounded && !gameOver) {
            playSound(forResource: "jump", ofType: "wav")
            let jumpUpAction = SKAction.moveBy(x: 0, y: 85, duration: 0.5)
            jumpUpAction.timingMode = .easeOut
            let jumpDownAction = SKAction.moveBy(x: 0, y: -85, duration: 0.5)
            jumpDownAction.timingMode = .easeIn
            let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
            
            isGrounded = false
            dinoPlayer!.component(ofType: SpriteComponent.self)?.node.run(jumpSequence, completion: {
                self.isGrounded = true
            })
            
        }
        if (gameOver && !isAddingMoney) {
//            restartGame()
        }
    }
    
    @objc func longPress(longPressGenstureRecognizer: UILongPressGestureRecognizer) {
        if longPressGenstureRecognizer.state == .began {
            if (isGrounded && !gameOver) {
                playSound(forResource: "jump", ofType: "wav")
                let jumpUpAction = SKAction.moveBy(x: 0, y: 150, duration: 0.6)
                jumpUpAction.timingMode = .easeOut
                let jumpDownAction = SKAction.moveBy(x: 0, y: -150, duration: 0.6)
                jumpDownAction.timingMode = .easeIn
                let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
                
                isGrounded = false
                dinoPlayer!.component(ofType: SpriteComponent.self)?.node.run(jumpSequence, completion: {
                    self.isGrounded = true
                })
            }
        }
    }
    
    func buttonPressed() {
        if (isGrounded && !gameOver) {
            playSound(forResource: "jump", ofType: "wav")
            let jumpUpAction = SKAction.moveBy(x: 0, y: 150, duration: 0.5)
            jumpUpAction.timingMode = .easeOut
            let jumpDownAction = SKAction.moveBy(x: 0, y: -150, duration: 0.5)
            jumpDownAction.timingMode = .easeIn
            let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
            
            isGrounded = false
            dinoPlayer!.component(ofType: SpriteComponent.self)?.node.run(jumpSequence, completion: {
                self.isGrounded = true
            })
        }
        
//        print(crouch)
    }
    
    func buttonReleased() {
//        print(crouch)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard touches.first != nil else {
            return
        }
    }
    
    func addScore() {
        if(gameOver){
            return
        }
        score += 10
        scoreIndicator.text = String(score)
    }
    
    func addSessionCoins(amount: Int) {
        if(gameOver) {
            return
        }
        sessionCoins += amount
        moneyIndicator.text = String(sessionCoins)+"¢"
    }
    
    func playSound(forResource name: String, ofType ext: String) {
        
        // get path to the sound
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: name, ofType: ext)!)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: sound)
            audioPlayer?.play()
        } catch {
            print("Audio error")
        }
    }
    
    func loseLive() {
        print("losing a life")
        playSound(forResource: "hurt", ofType: "wav")
        lives -= 1
        if(lives <= 0){
            livesImage.isHidden = true
            showRestartMenu(false)
        }
        else {
            livesImage.isHidden = false
            if(lives == 2){
                // turn eggs red temporarily to show damage
                livesImage.texture = SKTexture(imageNamed: "egg2.png")
                livesImage.size = CGSize(width: 54, height: 42)
                livesImage.position = CGPoint(x: livesImage.position.x, y: livesImage.position.y-10)
            }
            else{
                livesImage.texture = SKTexture(imageNamed: "egg1.png")
                livesImage.size = CGSize(width: 40, height: 50)
            }
        }
    }
    
    func addLive() {
        lives += 1
        livesImage.isHidden = false
        if(lives == 3){
            // turn eggs red temporarily to show damage
            livesImage.texture = SKTexture(imageNamed: "egg3.png")
            livesImage.size = CGSize(width: 54, height: 42)
            livesImage.position = CGPoint(x: livesImage.position.x, y: livesImage.position.y-10)
        }
        else if(lives == 2){
            // turn eggs red temporarily to show damage
            livesImage.texture = SKTexture(imageNamed: "egg2.png")
            livesImage.size = CGSize(width: 54, height: 42)
            livesImage.position = CGPoint(x: livesImage.position.x, y: livesImage.position.y-10)
        }
        else{
            livesImage.texture = SKTexture(imageNamed: "egg1.png")
            livesImage.size = CGSize(width: 40, height: 50)
        }
    }
    
    func showRestartMenu(_ won: Bool) {
        if gameOver {
            return;
        }
        gameOver = true
        entityManager.pauseComponents()
        timer.invalidate()
        background1.speed = 0
        background2.speed = 0
        let offset:CGFloat = -40
        if let spriteComponent = dinoPlayer!.component(ofType: SpriteComponent.self) {
            spriteComponent.node.removeAllActions()
        }
        dinoPlayer?.component(ofType: SpriteComponent.self)?.node.isPaused = true
        
        let label = SKLabelNode(fontNamed: "Courier-Bold")
        if(won) {
            label.fontSize = 15
        } else {
            label.fontSize = 30
        }
        label.fontColor = SKColor.black
        label.position = CGPoint(x: size.width/2, y: (size.height/3)*2+70)
        label.position.y += offset
        label.zPosition = 5
        label.verticalAlignmentMode = .center
        label.fontName = "8BITWONDERNominal"
        label.setScale(0)
        
        if self.score > self.bestScore {
            self.bestScore = self.score
        }
        label.text = "Best Score: \(self.bestScore)" // replace with fetch high score
        
    
//        let message2 = "Tap to Restart"
        
        prevAmmount = curMoney
        
        // update to firebase
        let data = ["money": curMoney + sessionCoins, "bestScore": self.bestScore]
        db.collection("Users").document(user!.uid).updateData(data)
        
        curAmmount = curMoney + sessionCoins
        
//        let label2 = SKLabelNode(fontNamed: "Courier-Bold")
//        label2.fontSize = 16
//        label2.fontColor = SKColor.black
//        label2.fontName = "8BITWONDERNominal"
//        label2.position = CGPoint(x: size.width/2, y: (size.height/3)*2+35)
//        label2.position.y += offset
//        label2.zPosition = 5
//        label2.verticalAlignmentMode = .center
//        label2.text = message2
//        label2.setScale(0)
        
        label3.fontSize = 15
        label3.fontColor = SKColor.black
        label3.position = CGPoint(x: size.width/2, y: (size.height/3)*2+100)
        label3.position.y += offset
        label3.zPosition = 5
        label3.verticalAlignmentMode = .center
        label3.fontName = "8BITWONDERNominal"
        
        playSound(forResource: "coin", ofType: "mp3")
        coinTimer = Timer.scheduledTimer(timeInterval: 0.06, target: self, selector: #selector(incrementCoins), userInfo: nil, repeats: true)
        
         // replace with fetch high score
        label3.setScale(0)
        
        addChild(label)
//        addChild(label2)
        addChild(label3)
        
        let scaleAction = SKAction.scale(to: 1.0, duration: 0.5)
        scaleAction.timingMode = SKActionTimingMode.easeInEaseOut
        label.run(scaleAction)
//        let scaleAction2 = SKAction.scale(to: 1.0, duration: 0.5)
//        scaleAction2.timingMode = SKActionTimingMode.easeInEaseOut
//        label2.run(scaleAction2)
        let scaleAction3 = SKAction.scale(to: 1.0, duration: 0.5)
        scaleAction3.timingMode = SKActionTimingMode.easeInEaseOut
        label3.run(scaleAction3)
        // add cool countup towards total coins, timeWait needed
    }
    
    @objc func incrementCoins() {
        if(prevAmmount <= curAmmount) {
            label3.text = "Total Coins: \(prevAmmount)"
            prevAmmount += 10
            isAddingMoney = true
        } else {
            isAddingMoney = false
            coinTimer.invalidate()
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        let deltaTime = currentTime - lastUpdateTimeInterval
        if spawnCooldown > 0 {
            spawnCooldown -= Float(deltaTime)
        } else if (gameOver == false){
            // trigger a speedup
//            speedMult += 0.5
            spawnCactus()
            spawnCooldown = Float(1 + Float.random(in: 0.3...1.4))
        }
        
        lastUpdateTimeInterval = currentTime
        entityManager.update(deltaTime)
        
        if let player = entityManager.dino(), let _ = player.component(ofType: DinoComponent.self) {
//            print("Dino Coins = \(dinoComp)")
        }
    }
    
}
