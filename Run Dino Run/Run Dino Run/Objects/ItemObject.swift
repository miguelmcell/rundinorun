//
//  ItemObject.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/7/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import Foundation

class ItemObject {
    var name:String
    var storeIndex:Int64
    
    init(name:String,storeIndex:Int64) {
        self.name = name
        self.storeIndex = storeIndex
    }
}
