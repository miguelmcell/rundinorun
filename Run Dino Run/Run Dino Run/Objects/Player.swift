//
//  Player.swift
//  Run Dino Run
//
//  Created by XCodeClub on 2020-05-05.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import Foundation

class Player {
    let userName: String
    var level: String
    var bestScore: Int
    //var color: String
    
    init(u: String, l: String, bs: Int) {
        userName = u
        level = l
        bestScore = bs
    }
    
    func getUserName() -> String {
        return userName
    }
    
    func getLevel() -> String {
        return level
    }
    
    func setLevel(newLevel: String) {
        level = newLevel
    }
    
    func getBestScore() -> Int {
        return bestScore
    }
    
    func setBestScore(newScore: Int) {
        bestScore = newScore
    }
}
