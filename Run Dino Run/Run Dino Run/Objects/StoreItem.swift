//
//  StoreItem.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/6/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import Foundation

class StoreItem {
    var itemName:String
    var itemImage:String
    var itemPrice:Int
    var isSold:Bool
    var itemCategory: String
    
    init(itemName: String, itemImage: String,itemPrice: Int, isSold: Bool, itemCategory: String) {
        self.itemName = itemName
        self.itemImage = itemImage
        self.itemPrice = itemPrice
        self.isSold = isSold
        self.itemCategory = itemCategory
    }
    
}
