//
//  StoreItemCellView.swift
//  Run Dino Run
//
//  Created by miguelmcell on 4/6/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit

class StoreItemCellView: UIView {
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.black.cgColor
    }
    

}
