//
//  Utilities.swift
//  Run Dino Run
//
//  Created by Jianbin Lin on 3/30/20.
//  Copyright © 2020 Jianbin Lin. All rights reserved.
//

import UIKit
import FirebaseAuth

class Utility {
    
    static func createAlert(on vc: UIViewController, _ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // create buttons
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func showToast(on vc: UIViewController, message: String, duration: Double) {
        let toast = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        toast.view.alpha = 0.9
        toast.view.layer.cornerRadius = 15
        
        vc.present(toast, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() +  duration) {
            toast.dismiss(animated: true, completion: nil)
        }
    }
    
    static func transitionToVC(to nextViewController: String, from preViewController: UIViewController, animated: Bool = true) {
        let VC = (preViewController.storyboard?.instantiateViewController(identifier: nextViewController))!
        
        preViewController.navigationController?.isNavigationBarHidden = true
        
        preViewController.navigationController?.pushViewController(VC, animated: animated)
    }
    
    static func signIn(from vc: UIViewController, withEmail email: String, password: String, firstLogIn: Bool, dinoImage: UIImageView!){
        
        showToast(on: vc, message: "Logging in", duration: 0.5)
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            
            // sign in fail
            if error != nil {
                createAlert(on: vc, "Error", error!.localizedDescription)
                return
            }
            // save for auto login
            if firstLogIn {
                UserDefaults.standard.set(email, forKey: "email")
                UserDefaults.standard.set(password, forKey: "password")
            }
            // animation
            dinoImage.image = UIImage(named: "dino")
            dinoImage.frame = CGRect(x: 95, y: 605, width: 225, height: 185)
            dinoImage.startAnimating()
            UIView.animate (
                withDuration: 1.0,
                delay: 0.0,
                options: .curveEaseOut,
                animations: {
                    dinoImage.frame.origin.x += dinoImage.frame.size.width
                }
            )
            // transition to home screen
            transitionToVC(to: "TabBarController", from: vc)
            guard let user = result?.user.displayName else {
                return
            }
            showToast(on: vc, message: "Welcome \(user)", duration: 2.0)
        }
    }
    
}
